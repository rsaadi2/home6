import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
//peaklass
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();

    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        System.out.print(g);
        System.out.println("Esimese testi center on järgmised tipud: ");
        g.listToString(g.leiaCenter());


        Graph f = new Graph("F");
        f.createRandomSimpleGraph(6, 9);
        System.out.print(f);
        System.out.println("Teise testi center on järgmised tipud: ");
        g.listToString(f.leiaCenter());

        Graph h = new Graph("H");
        h.createRandomSimpleGraph(6, 9);
        System.out.print(h);
        System.out.println("Kolmanda testi center on järgmised tipud: ");
        h.listToString(h.leiaCenter());

        Graph i = new Graph("I");
        i.createRandomSimpleGraph(6, 9);
        System.out.print(i);
        System.out.println("Neljanda testi center on järgmised tipud: ");
        i.listToString(i.leiaCenter());

        Graph j = new Graph("J");
        j.createRandomSimpleGraph(6, 9);
        System.out.print(j);
        System.out.println("Viienda testi center on järgmised tipud: ");
        j.listToString(j.leiaCenter());



        // TODO!!! Your experiments here
    }

    // TODO!!! add javadoc relevant to your problem
   /*  - " -. lk. 20, ül. 15 - koostada meetod etteantud sidusa lihtgraafi G tsentri leidmiseks.
  */
    // tipu klass
    class Vertex {

        //kuna saame individuaalse ülesande, siis võime oma vajadustele vastavalt siia väju
        //juurde lisada.

        private String id;
        private Vertex next;//viit järgmisele tipule, jooksevad ülevalt alla, g, a, b, c
        private Arc first; // viit esimesele väljuvale kaarele
        private int info = 0;
        private int eccentricity;

        // You can add more fields, if needed

        Vertex(String s, int ecc) {
            id = s;
            eccentricity = ecc;
        }

        //konstruktor
        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        // hetkel tagastab liht id, ent kui meil ülesandes mingit infot vaja välja
        //trükkida,siiis tuleks see panna siia.
        public String toString() {
            return id;
        }

        // TODO!!! Your Vertex methods here!
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    // kaare klass
    class Arc {

        private String id;
        private Vertex target;//viit kaarest tagasi tippudesse ab to b,ac to c, ad to d
        private Arc next; //vasakult paremale viit, a, ab, ac, ad
        private int info = 0;

        // You can add more fields, if needed


        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        // TODO!!! Your Arc methods here!
    }


    class Graph {

        private String id;//identifikaator
        private Vertex first; //viit esimesele tipule. viit on null, kui tegu tühja graafiga
        private int info = 0; //infoväli
        private List<Vertex> tipud = new ArrayList<>();
        private List<Vertex> centerTipud = new ArrayList<>();
        // You can add more fields, if needed


        //konstruktor
        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        //8.30 räägib sellest: 10 okt. tahame anda kogu komplektile mingit stringi kuju.
        // seda nimetatakse külgnevusstruktuuri läbikäimiseks - SEE ei ole graafi läbikäimine.
        public String toString() {
            String nl = System.getProperty("line.separator");//lineseperator - reavahetus, ei mahu ühele ära
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id); //vastab sellele, mis graphi päises.id tähendab this.id, kuna selel klassi id
            sb.append(nl);//see on süsteemne reavahetus
            Vertex v = first;
            while (v != null) {//see tsükkel jookseb üle tippude, kui ei ole tippu, ei juhtu midagi
                sb.append(v.toString()); //pöördume tipu tostring meetodi poole, hetkel seal aint tipu nime väljatrükk
                sb.append(" -->");//seejärellisame noolekse, ehk N: A -->
                Arc a = v.first; //ja seejärel käime läbi kaarte ahela
                while (a != null) {
                    sb.append(" ");//kui leidub ahel, siis paneme tühiku.
                    sb.append(a.toString()); //kaare nimi, N: AB, ehk A --> AB
                    sb.append(" (");//kirjutame avava sulu AB, ehk A --> AB (
                    sb.append(v.toString()); // uuesti tipu nimi
                    sb.append("->");// uus nool: A --> AB (A ->
                    sb.append(a.target.toString());// kaare suubumine, ehk B
                    sb.append(")"); //ja sulg kinni ehk A --> AB (A->B)
                    a = a.next; //ja võtame järgmise elemendi A --> AB (A->B) AC(A ->C)
                    //kuniks see tsükkel lõppeb ja lähme uue tipu juurde.
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            tipud.add(res);
            centerTipud.add(res);
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
      /*selle näite puhul ei ole lisamälu võetud.Käiakse tipud läbi. igast tipust kaared läbi
      * ja elementi (I)(j) suurendatakse 1 võrra. */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;

        }

        /**
         * Converts adjacency matrix to the matrix of distances.
         *
         * @return matrix of distances, where each edge has length 1
         * Konverteerib külgnevusmaatriksist kaugusmaatriksiks. Kui tippude vahel on kaar, siis väärtus 1
         * Kui tippude vahel ei ole kaart, siis väärtus infinity(Integer.MAX_VALUE(jagame kahega, kuna kui liita
         * 1 juurde, siis jookseb kokku)
         */
        public int[][] leiaKaugusMaatriks() {

            int vertices = this.tipud.size();

            int[][] resultMatrix = new int[vertices][vertices];
            int[][] tempMatrix = this.createAdjMatrix();
            int lopmatus = Integer.MAX_VALUE / 2;


            if (tipud.size() < 1) {
                throw new RuntimeException("Graafil ei saa olla vähem, kui 1 tipp");
            }

            for (int i = 0; i < vertices; i++) {
                for (int j = 0; j < vertices; j++) {
                    if (tempMatrix[i][j] == 0) {
                        resultMatrix[i][j] = lopmatus;
                    } else resultMatrix[i][j] = 1;
                }

            }
            for (int k = 0; k < vertices; k++) {
                resultMatrix[k][k] = 0;
            }

            return resultMatrix;
        }


        /**
         * Calculates shortest paths using Floyd-Warshall algorithm.
         * This matrix will contain shortest paths between each pair of vertices.
         * Ehk leiame lühimad teed, mille järgi saan leida ka centri.
         */
        public ArrayList<Vertex> leiaCenter() {

            int n = this.tipud.size();
            int minKaugus = Integer.MAX_VALUE / 2;



            ArrayList<Vertex> vertexiteList = new ArrayList<Vertex>();

            int[][] resultMatrix = this.leiaKaugusMaatriks();


            if (n < 1) throw new RuntimeException("Tippe on liiga vähe");

// siin me setime kaugused tip tipu suhtes.
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        int uusKaugus = resultMatrix[i][k] + resultMatrix[k][j];
                        if (uusKaugus < resultMatrix[i][j]) {
                            resultMatrix[i][j] = uusKaugus;
                        }
                    }
                }

            }
            //Selleks hetkeks on meil nüüd täielik kaugusmaatriks iga punkti suhtes.


            // käin read läbi ja salvestan suurima
            int reaSuurim = 0;
            String vertexId;
            for (int g = 0; g < n; g++) {
                for (int l = 0; l < n; l++) {
                    if (resultMatrix[g][l] > reaSuurim) {
                        reaSuurim = resultMatrix[g][l];
                    }
                }
                int temp = g+1;
                vertexId = "v" + temp;

                Vertex uusVertex = new Vertex(vertexId, reaSuurim);
                vertexiteList.add(uusVertex);
                reaSuurim = 0;
            }

            //nüüd mul olemas iga rea maximumid, neist vaja leida miinimumid, mis
            //moodustavad centri


            int miinimum = 99;
            int eccentr;



//leian miinimum eccentrity
                for (int i = 0; i < vertexiteList.size(); i++) {
                    eccentr = vertexiteList.get(i).eccentricity;
                    if (eccentr < miinimum) {
                        miinimum = vertexiteList.get(i).eccentricity;

                    }
                }
//kustutan listist need, mis suurem, kui miinimum
           for(int j=0; j<vertexiteList.size(); j++){
               if (vertexiteList.get(j).eccentricity > miinimum) {
                   vertexiteList.remove(j);
                   j--;
               }
           }



            return vertexiteList;

        }

        public void listToString(List<Vertex> vertex){
            for (int i = 0; i < vertex.size(); i++) {

                System.out.println(vertex.get(i).toString());
            }
        }







        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        // TODO!!! Your Graph methods here! Probably your solution belongs here.
    }

} 

